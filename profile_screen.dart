import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(15.0, 5.0, 15.0, 5.0),
                child: Icon(Icons.account_circle_sharp, size: 100),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: Divider(color: Colors.grey),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: Text("Name"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: TextFormField(initialValue: "Jane"),
              ),
              // SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: Text("Surname"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: TextFormField(initialValue: "Doe"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: Text("Phone Number"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: TextFormField(initialValue: "+27 62 123 4567"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child: Text("Password"),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 5.0),
                child:
                    TextFormField(obscureText: true, initialValue: "password"),
              ),
              SizedBox(height: 25),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                        color: Colors.amber,
                        borderRadius: BorderRadius.circular(7),
                      ),
                      child: Center(
                          child: Text(
                        "Save",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ))),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
