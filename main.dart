import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:sinoxolo_simelane_module_4/login_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AnimatedSplashScreen(
        splash: Container(
          color: Colors.amber,
          child: Icon(
            Icons.ac_unit_sharp,
            size: 50,
          ),
        ),
        nextScreen: LoginScreen(),
        splashTransition: SplashTransition.rotationTransition,
        duration: 3000,
      ),
    );
  }
}

